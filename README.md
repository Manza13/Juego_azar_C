# Juego_azar_C
---> Español

Juego del azar programado en C, para el grupo de CSEC de telegram, donde se esta aprendiendo a programar en C. Aqui muestro una de mis creaciones que suelen ser lo mas comun de hacer en cualquier lenguaje de programacion en el cual no se suele tardar, mucho en esta ocasion o muestro un juego. Se trata de usar el metodo "FOR" para realizar bucles.

Este juego trata principalmente de que nos sale un numero aleatorio entre el 1 y el 100, el usuario debera adivinar cual es el numero correcto, mientras que tiene 10 intentos para adivinarlo, en caso de que vaya fallando el mismo juego le indicara si el numero que ha sido introducido es mas pequeño o mas mayor al nuemero correspondiente:

ejemplo:

> Introduce un numero entre el 1 y el 100: 50. 
> El numero introducido es demasiado pequeño.
> Introduce un numero entre el 1 y el 100: 98.
> El numero introducido es demasiado grande.

En este codigo que os dejo se encontrara todo explicado de cada una de las lineas, del propio juego.
Gracias a la ayuda de @Secury de telegram por corregirme en algunas explicaciones. 


---> English:

Game of random programmed in C, for the group of CSEC of Telegram, where one is learning to program in C. Here I show one of my creations that are usually the most common thing to do in any language of programming in which does not usually take , a lot on this occasion or show a game. It's about using the "for" method to make loops.

This game mainly deals with that we get a random number between 1 and 100, the user should guess which is the correct number, while has 10 attempts to guess, in case you go failing the same game will indicate if the number that has been introdu is smaller or higher than the corresponding number:

Example:

> Enter a number between 1 and 100: 50.
> The number entered is too small.
> Enter a number between 1 and 100: 98.
> The number entered is too large.

In this code that I leave you will find everything explained from each of the lines, the game itself.
Thanks to the help of @Secury of Telegram to correct me in some explanations.
